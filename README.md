# Interview Manager
This solution is an application which allows the user to save interviews in a local storage of browser.

## Requirements
To launch this solution, you must have this tools
1. Node.js version 6 or above
2. npm version 3 or above (You can use yarn instead if you feel at ease)

## Test Installation Deployment
Here are different commands of this application 
1. `npm install`. This command will install all packages as dependencies.
2. `npm start` or `gulp` This command will use _browser-sync_ server to launch the application in the port 4001 of localhost.
3. `npm run build` or `gulp build`. This command will build the dist folder. The dist folder is destined to be used in the deployment process. 
4. `gulp lint` This command will use ESlint configuration to summary warnings and errors in JavaScript code.
5. `npm test` This command will launch all tests.

## Architecture
The main framework of this solution is _angularjs 1.6.5_. The language specification is ES6. I associate it with [babel](https://babeljs.io/) to transcribe the distribution from ES6 to ES5. So the solution will work on olds browsers.

### Why ES6?
ES6 brings much news in JavaScript and allows me write better code. With ES6 the code is much more readable and easily testable. I no longer have to use external libraries such as lodash.
