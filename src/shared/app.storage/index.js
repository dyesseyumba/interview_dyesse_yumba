import angular from 'angular';
import StorageFactory from './storage.factory';

// Create the app.storage module
angular
  .module('app.storage', [])
  .factory('StorageFactory', ($window) => {
    'ngInject';
    return new StorageFactory($window)
  });
