class StorageFactory {
  constructor($window) {
    'ngInject';

    this.storage = $window.localStorage;
    this.interviews = (this.storage["interviews"] !== undefined && Array.isArray(JSON.parse(this.storage["interviews"])))
      ? [...JSON.parse(this.storage["interviews"])]
      : [];
  }

  save(interview) {

    this.interviews.push(interview);

    this.storage.setItem("interviews", JSON.stringify(this.interviews));
  }

  get() {
    return this.interviews;
  }
}

export default StorageFactory;
