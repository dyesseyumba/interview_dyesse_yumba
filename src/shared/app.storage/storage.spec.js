import '../../app';
import 'angular-mocks';

// import StorageFactory from './storage.factory';

/* eslint-disable no-undef */
describe('Storage factory', () => {
  let StorageFactory;

  beforeEach(angular.mock.module('app'));
  beforeEach(angular.mock.inject((_$window_, _StorageFactory_) => {
    StorageFactory = _StorageFactory_;
  }));

  it('Should save the interview', () => {
    StorageFactory.save({id: "#123456ASDFTGH"});

    expect(localStorage["interviews"]).toBeDefined();
  });

  it('Should get the interviews', () => {

    StorageFactory.save({id: "#123456ASDFTGH"});
    expect(StorageFactory.get()).toBeDefined();
  });

});
