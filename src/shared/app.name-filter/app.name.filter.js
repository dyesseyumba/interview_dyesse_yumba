class NameFilter {
  static filter(names) {

    if (names.length > 12) {

      const nameArray = names.split(' ');

      let result = nameArray[0] + ' '

      if (nameArray[0].length >= 2) {
        for (let i = 1; i <= nameArray.length - 1; i++) {

          result += nameArray[i]
            .substring(0, 1)
            .toUpperCase() + '. ';
        }
      }

      return result;
    }

    return names;

  }
}

export default NameFilter.filter;
