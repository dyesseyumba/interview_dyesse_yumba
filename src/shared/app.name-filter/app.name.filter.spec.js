import '../../app';
import 'angular-mocks';

import nameFilter from './app.name.filter';

/* eslint-disable no-undef */
describe('InterviewList', () => {

  it('Should generate new ID', () => {

    expect(nameFilter("")).toBeDefined(14);
    expect(nameFilter("Test").length).toBeDefined(4);
    expect(nameFilter("Test Test Test").length).toBeDefined(6);


  });

});
