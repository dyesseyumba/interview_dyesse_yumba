import angular from 'angular';
import nameFilter from './app.name.filter';

// Create the 'app.name-filter' module
angular
  .module('app.name-filter', [])
  .filter('nameFilter', () => nameFilter);
