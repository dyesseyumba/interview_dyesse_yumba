import angular from 'angular';
import PhoneNumberValidator from './phone-number-validator';

// Create the 'app.phoneNumberValidator' module
angular
  .module('app.phoneNumberValidator', [])
  .directive('phoneNumber', () => new PhoneNumberValidator());
