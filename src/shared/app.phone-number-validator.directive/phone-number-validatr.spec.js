import '../../app';
import 'angular-mocks';

/* eslint-disable no-undef */
describe('PhoneNumberValidator', () => {
  let $scope,
    form;

  beforeEach(angular.mock.module('app.id-generator'));
  beforeEach(inject(function ($compile, $rootScope) {
    $scope = $rootScope;
    var element = angular.element('<form name="form"><input type="text" name="intervieweePhone"ng-model="interview.' +
        'intervieweePhoneNumber" name="intervieweePhone"  phone-number required></form>');
    $scope.interview = {
      intervieweePhoneNumber: null
    }
    $compile(element)($scope);
    form = $scope.form;
  }));

  describe('phone-number', function () {
    it('should pass with valid number', function () {
      form.intervieweePhone.$setViewValue('565-984-9545');
      $scope.$digest();
      expect($scope.interview.intervieweePhoneNumber).toEqual('565-984-9545');
      expect(form.intervieweePhone.$valid).toBe(true);
    });
  });
});
