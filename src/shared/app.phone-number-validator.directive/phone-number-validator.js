/**
 *  The directive to validate phone number
 *
 * @class PhoneNumberValidator
 */
class PhoneNumberValidator {
  constructor() {
    this.restrict = 'A';
    this.require = 'ngModel';
  }
  link($scope, $element, $attrs, $ctrl) {
    const PHONE_REGEX = /^-?\d{3}-\d{3}-\d{4}$/;
    $ctrl.$validators.phoneNumber = function (modelValue, viewValue) {

      if (PHONE_REGEX.test(viewValue)) {
        return true;
      }
      return false;
    };
  }
}

export default PhoneNumberValidator;
