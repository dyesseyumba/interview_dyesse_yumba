import angular from 'angular';
import IdGeneratorFactory from './id-generator.factory';

// Create the app.id-generator module to generate unique ID
angular.module('app.id-generator', [])
  .factory('IdGeneratorFactory', () => new IdGeneratorFactory());
