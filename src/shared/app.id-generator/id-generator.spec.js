import '../../app';
import 'angular-mocks';

/* eslint-disable no-undef */
describe('IdGeneratorFactory', () => {
  let IdGeneratorFactory;

  beforeEach(angular.mock.module('app.id-generator'));
  beforeEach(angular.mock.inject((_IdGeneratorFactory_) => {
    IdGeneratorFactory = _IdGeneratorFactory_;
  }));

  it('Should generate new ID', () => {
    const id1 =IdGeneratorFactory.nextId();

    expect(id1.length).toEqual(14);
    expect(id1.substring(0,1)).toEqual('#');


  });

});
