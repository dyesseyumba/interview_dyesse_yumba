/**
 *  The factory to generate unique interview ID
 *
 * @class IdGeneratorFactory
 */
class IdGeneratorFactory {

  /**
   * Call to generate next unique id
   *
   * @returns {string} unique id
   * @memberof IdGeneratorFactory
   */
  nextId() {
    const pwLength = 13;
    const allowChar = 'ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789';
    let randomId = "#";

    for (let i = 0; i < pwLength; i++) {
      randomId = randomId + allowChar[Math.floor(Math.random() * allowChar.length)];
    }

    return randomId;
  }

}

export default IdGeneratorFactory;
