import '../../app';
import 'angular-mocks';

import DatePickerComponent from './datepicker.component';
import DatePickerController from './datepicker.controller';

/* eslint-disable no-undef */
describe('DatePicker', () => {
  beforeEach(angular.mock.module('app'));

  //The DatePickerComponent
  describe('DatePickerComponent', () => {
    const component = DatePickerComponent;

    it('Should include template url', () => {
      expect(component.templateUrl).toEqual('shared/app.datepicker/datepicker.html');
    });

    it("should include controller named 'DatePickerController'", () => {
      expect(component.controller).toEqual('DatePickerController');
    });

     it("should include bindings", () => {
      expect(component.bindings.date).toEqual('<');
      expect(component.bindings.onDayChanged).toEqual('&');
      expect(component.bindings.onMonthChanged).toEqual('&');
      expect(component.bindings.onYearChanged).toEqual('&');
    });
  });

  // The DatePickerController
  describe('DatePickerController', () => {

    let makeController;
    beforeEach(angular.mock.inject((_AppConstants_) => {
      makeController = () => {
        return new DatePickerController(_AppConstants_);
      };
    }));

    it('Controller should be defined', () => {
      let datePickerController = makeController();

      expect(datePickerController).toBeDefined();
      expect(datePickerController.months.length).toEqual(12);
    });

  });
});
