/**
 * The controller of appDatepicker module
 *
 * @class DatePickerController
 */
class DatePickerController {
  constructor(AppConstants) {
    'ngInject';

    this.months = AppConstants.months;
  }

}
export default DatePickerController;
