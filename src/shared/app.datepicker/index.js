import angular from 'angular';
import datepicker from './datepicker.component';
import datepickerController from './datepicker.controller';

//Define module 'app.datepicker' and its component
angular.module('app.datepicker', [])
  .controller('DatePickerController', datepickerController)
  .component('appDatepicker', datepicker);
