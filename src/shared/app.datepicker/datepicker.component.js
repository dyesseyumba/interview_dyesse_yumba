/**
 * The datepicker component
 */
const datePicker = {
  bindings: {
    date: '<',
    onDayChanged: '&',
    onMonthChanged: '&',
    onYearChanged: '&'
  },
  templateUrl: 'shared/app.datepicker/datepicker.html',
  controller: 'DatePickerController'
}

export default datePicker;
