/**
 * The Header component
 */
const header = {
  templateUrl: 'layout/header/header.html'
}

export default header;
