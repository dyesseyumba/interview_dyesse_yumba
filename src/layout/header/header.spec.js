import '../../app';

import FooterComponent from './header.component';

/* eslint-disable no-undef */
describe('FooterComponent', () => {

  const component = FooterComponent;

  it('Should include template url', () => {
    expect(component.templateUrl).toEqual('layout/header/header.html');

  });
});
