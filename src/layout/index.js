import angular from 'angular';
import header from './header/header.component';
import footer from './footer/footer.component';

//Define module 'app.layout' and its component
angular.module('app.layout', []);

//The interview-form component
angular
  .module('app.layout')
  .component('appHeader', header)
  .component('appFooter', footer);
