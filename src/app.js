import angular from 'angular';
import '@uirouter/angularjs';


// Import app config files
import constants from './config/app.constants';
import appConfig from './config/app.config';
import appRun from './config/app.run';

// Import templates file (generated by Gulp)
import './config/app.templates';

// Import app functionality
import './layout'
import './interview';
import './shared/app.datepicker';
import './shared/app.id-generator';
import './shared/app.storage';
import './shared/app.phone-number-validator.directive';
import './shared/app.name-filter';

// Create and bootstrap application
const requires = [
  'ui.router',
  'templates',
  'app.interview',
  'app.datepicker',
  'app.id-generator',
  'app.storage',
  'app.layout',
  'app.phoneNumberValidator',
  'app.name-filter'
];

angular.module('app', requires);

angular.module('app').constant('AppConstants', constants);

angular.module('app').config(appConfig);

angular.module('app').run(appRun);
