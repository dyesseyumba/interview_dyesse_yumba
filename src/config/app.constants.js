/**
 * Constants of whole application
 */
const AppConstants = {
  appName: 'Interview Store',
  months: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ],
}

export default AppConstants;
