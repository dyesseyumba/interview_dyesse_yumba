
/**
 * Initialize application before start
 *
 * @param {object} $rootScope
 * @param {object} $state
 * @param {object} $stateParams
 */
const AppRun = ($rootScope, $state, $stateParams) => {
  'ngInject';

  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

}

export default AppRun;
