
/**
 * Initial setup of the whole app
 *
 * @param {object} AppConstants
 * @param {object} $stateProvider
 * @param {object} $urlRouterProvider
 */
const AppConfig = (AppConstants, $stateProvider, $urlRouterProvider) => {
  'ngInject';


  const homeState = {
    name: 'home',
    url: '/',
    component: 'interviewForm',
    data: {
      pageTitle: 'Home - ' + AppConstants.appName
    }
  }

  const interviewListState = {
    name: 'interviewList',
    url: '/interviews',
    component: 'interviewList',
    data: {
      pageTitle: 'Interviews - ' + AppConstants.appName
    }
  }
  $stateProvider.state(homeState);
  $stateProvider.state(interviewListState);


  $urlRouterProvider.otherwise('/');

}

export default AppConfig;
