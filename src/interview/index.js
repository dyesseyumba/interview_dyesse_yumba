import angular from 'angular';
import interviewForm from './form/interview-form.component';
import interviewFormController from './form/interview-from.controller';
import interviewList from './list/interview-list.component';
import interviewListController from './list/interview-list.controller';

//Define module 'app.interview' and its component
angular.module('app.interview', []);

//The interview-form component
angular
  .module('app.interview')
  .controller('InterviewFormController', interviewFormController)
  .component('interviewForm', interviewForm)

//The interview-list component
angular
  .module('app.interview')
  .controller('InterviewListController', interviewListController)
  .component('interviewList', interviewList)
