/**
 * The controller of interviewList component
 *
 * @class InterviewListController
 */
class InterviewListController {
  constructor(StorageFactory) {
    'ngInject';

    this.interviews = StorageFactory.get();
  }
}

export default InterviewListController;
