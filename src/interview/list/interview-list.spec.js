import '../../app';
import 'angular-mocks';

import InterviewListComponent from './interview-list.component';
import InterviewListController from './interview-list.controller';

/* eslint-disable no-undef */
describe('InterviewList', () => {
  beforeEach(angular.mock.module('app'));

  //The InterviewListComponent
  describe('InterviewListComponent', () => {
    const component = InterviewListComponent;

    it('Should include template url', () => {
      expect(component.templateUrl).toEqual('interview/list/interview-list.html');
    });

    it("should include controller named 'InterviewListController'", () => {
      expect(component.controller).toEqual('InterviewListController');
    })
  });

  // The InterviewListController
  describe('InterviewListController', () => {

    let makeController;
    beforeEach(angular.mock.inject((_StorageFactory_) => {
      makeController = () => {
        return new InterviewListController(_StorageFactory_);
      };
    }));

    it('Controller should be defined', () => {
      let interviewListController = makeController();

      expect(interviewListController).toBeDefined();
    });

  });
});
