/**
 * The interviewList component
 */
const interviewList = {
  templateUrl:'interview/list/interview-list.html',
  controller: 'InterviewListController'
}

export default interviewList;
