/**
 * The interviewForm component
 */
const interviewForm = {
  templateUrl:'interview/form/interview-form.html',
  controller: 'InterviewFormController'
}

export default interviewForm;
