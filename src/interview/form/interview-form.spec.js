import '../../app';
import 'angular-mocks';

import InterviewFormComponent from './interview-form.component';
import InterviewFormController from './interview-from.controller';

/* eslint-disable no-undef */
describe('InterviewForm', () => {
  beforeEach(angular.mock.module('app'));

  //The InterviewFormComponent
  describe('InterviewFormComponent', () => {
    const component = InterviewFormComponent;

    it('Should include template url', () => {
      expect(component.templateUrl).toEqual('interview/form/interview-form.html');
    });

    it("should include controller named 'InterviewFormController'", () => {
      expect(component.controller).toEqual('InterviewFormController');
    })
  });

  // The InterviewFormController
  describe('InterviewFormController', () => {

    let makeController;
    beforeEach(angular.mock.inject((_AppConstants_, _IdGeneratorFactory_, _StorageFactory_) => {
      makeController = () => {
        return new InterviewFormController(_AppConstants_, _IdGeneratorFactory_, _StorageFactory_);
      };
    }));

    //selectedCity method
    describe('selectedCity', () => {

      it("'Elsewhere' selected, city should equal ''", () => {

        let interviewFormController = makeController();
        interviewFormController.interview.city = "Elsewhere";

        interviewFormController.selectedCity();

        expect(interviewFormController.selectedElsewhere).toBe(true);
        expect(interviewFormController.interview.city).toEqual("");
      });

      it("'Elsewhere' is not selected, city should be the selected one", () => {

        let interviewFormController = makeController();
        interviewFormController.interview.city = "London";

        interviewFormController.selectedCity();

        expect(interviewFormController.selectedElsewhere).toBe(false);
        expect(interviewFormController.interview.city).toEqual("London");
      });
    });

    //onDayChanged methods
    it('onDayChanged should change the day of dt field', () => {

      let interviewFormController = makeController();
      interviewFormController.dt = new Date(2017, 5, 5);
      interviewFormController.onDayChanged(5);

      expect(interviewFormController.dt.getDate()).toBe(5);
    });

    //onDayChanged methods
    it('onMonthChanged should change the month of dt field', () => {

      let interviewFormController = makeController();
      interviewFormController.dt = new Date(2017, 5, 5);
      interviewFormController.onMonthChanged('Mar');

      expect(interviewFormController.dt.getMonth()).toBe(2);
    });

    //onDayChanged methods
    it('onYearChanged should change the year of dt field', () => {

      let interviewFormController = makeController();
      interviewFormController.dt = new Date(2017, 5, 5);
      interviewFormController.onYearChanged(2015);

      expect(interviewFormController.dt.getFullYear()).toBe(2015);
    });

    it('generateId should set th new id', () => {

      let interviewFormController = makeController();
      interviewFormController.interview.id = undefined;
      interviewFormController.generateId();

      expect(interviewFormController.interview.id).toBeDefined();
      expect(interviewFormController.interview.id.length).toEqual(14);
    });

    it("UpdateDate should update interview's date", () => {

      let interviewFormController = makeController();
      interviewFormController.updateDate(new Date(2015,9,9));

      expect(interviewFormController.interview.date).toEqual(new Date(2015,9,9));
    });

    describe('submit', () => {
      it('Should submit when validated', () => {
        let interviewFormController = makeController();
        interviewFormController.form = {
          $valid: true,
          $setPristine: () => null,
          $setUntouched: () => null,
          $submitted: true
        };

        interviewFormController.submit();

        expect(interviewFormController.submitSucceeded).toBe(true);
        expect(interviewFormController.interview.date.getMonth()).toEqual(new Date().getMonth());

      });

       it("Shouldn't submit when validated", () => {
        let interviewFormController = makeController();
        interviewFormController.form = {
          $valid: false,
          $setPristine: () => null,
          $setUntouched: () => null,
          $submitted: true
        };

        interviewFormController.submit();

        expect(interviewFormController.submitSucceeded).toBe(false);
        expect(interviewFormController.form.$submitted).toBe(true);

      });
    });

    it('Should close the alert', () => {

      let interviewFormController = makeController();
      interviewFormController.closeAlert();

      expect(interviewFormController.submitSucceeded).toBe(false);
    });
  });
});
