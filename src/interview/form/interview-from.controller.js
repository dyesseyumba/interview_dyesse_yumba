/**
 * The controller of interviewForm component
 *
 * @class InterviewFormController
 */
class InterviewFormController {
  constructor(AppConstants, IdGeneratorFactory, StorageFactory) {
    'ngInject';

    this.selectedElsewhere = false;
    this.interview = {date: new Date()};
    this.dt = new Date();
    this.date = {
      day: this.dt.getDate(),
      month: AppConstants.months[this.dt.getMonth()],
      year: this.dt.getFullYear()
    };
    this.AppConstants = AppConstants;
    this.IdGeneratorFactory = IdGeneratorFactory;
    this.StorageFactory = StorageFactory;
    this.purposes = ["Visa", "Permanent Residence"];
    this.cities = ["London", "Brighton", "Belfast", "Cardiff", "NewCastle", "Elsewhere"];
    this.submitSucceeded = false;
  }

  /**
   * Display input for city when Elsewhere is selected in city select
   *
   * @memberof InterviewFormController
   */
  selectedCity() {
    this.selectedElsewhere = (this.interview.city == "Elsewhere") ? true : false;

    this.interview.city = (this.interview.city == "Elsewhere") ? "" : this.interview.city;
  }

  /**
   * Change the day of interview date
   *
   * @param {number} day
   * @memberof InterviewFormController
   */
  onDayChanged(day) {
    this.dt.setDate(day);
    this.updateDate(this.dt);
  }

  /**
   * Change the month of interview date
   *
   * @param {string} month
   * @memberof InterviewFormController
   */
  onMonthChanged(month) {

    this.dt .setMonth(this.AppConstants.months.indexOf(month));
    this.updateDate(this.dt);
  }

  /**
   * Change the year of interview date
   *
   * @param {number} year
   * @memberof InterviewFormController
   */
  onYearChanged(year) {

    this.dt.setFullYear(year);
    this.updateDate(this.dt);
  }

  /**
   * Update the interview date
   *
   * @param {date} date
   * @memberof InterviewFormController
   */
  updateDate(date) {
    const day = date.getDate(),
          month=this.AppConstants.months[date.getMonth()],
          year=date.getFullYear();

    this.date = {
      day: day,
      month: month,
      year :year
    };
    this.interview.date = new Date(year, this.AppConstants.months.indexOf(month), day);
  }

  /**
   * Generate the unique identifier for interview
   *
   * @memberof InterviewFormController
   */
  generateId() {
    this.interview.id = this.IdGeneratorFactory.nextId();
  }

  /**
   * Use the StorageFactory to save the interview in the store
   *
   * @memberof InterviewFormController
   */
  submit() {

    if(this.form.$valid){

      this.StorageFactory.save(this.interview);
      this.submitSucceeded = true;
      this.interview = {date : new Date()};

      this.form.$setPristine();
      this.form.$setUntouched();
      this.form.$submitted = false;
    }
  }

  /**
   * Close the success alert
   *
   * @memberof InterviewFormController
   */
  closeAlert(){
    this.submitSucceeded = false;
  }
}

export default InterviewFormController;
