var gulp = require('gulp'),
  newer = require('gulp-newer'),
  notify = require('gulp-notify'),
  source = require('vinyl-source-stream'),
  browserify = require('browserify'),
  babelify = require('babelify'),
  ngAnnotate = require('browserify-ngannotate'),
  browserSync = require('browser-sync').create(),
  rename = require('gulp-rename'),
  templateCache = require('gulp-angular-templatecache'),
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),
  merge = require('merge-stream'),
  eslint = require('gulp-eslint'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  uglifycss = require('gulp-uglifycss');

// Where our files are located
var jsFiles = 'src/**/*.js',
  viewFiles = 'src/**/*.html',
  dest = './build/',
  src = './src/',
  dist = './dist/',
  sassFiles = [
    src + 'sass/app.scss',
    'node_modules/angular/angular-csp.css'
  ];

var interceptErrors = function () {
  var args = Array
    .prototype
    .slice
    .call(arguments);

  // Send error to notification center with gulp-notify
  notify
    .onError({title: 'Compile Error', message: '<%= error.message %>'})
    .apply(this, args);

  // Keep gulp from hanging on this task
  this.emit('end');
};

//Ue browserify to manage js file ans transcript ES6 to ES6 with babel
gulp.task('browserify', [ 'views', 'lint'], function () {
  return browserify(src + 'app.js').transform(babelify, {
    presets: [
      [
        'env', {
          targets: {
            browsers: ['last 2 versions', 'ie >= 11']
          },
            loose: true
          }
        ]
    ]
  })
    .transform(ngAnnotate)
    .bundle()
    .on('error', interceptErrors)
    //Pass desired output filename to vinyl-source-stream
    .pipe(source('main.js'))
    // Start piping stream to tasks!
    .pipe(gulp.dest(dest));
});

gulp.task('html', function () {
  return gulp
    .src('src/index.html')
    .on('error', interceptErrors)
    .pipe(newer(dest+'index.html'))
    .pipe(gulp.dest(dest));
});

// manage images
gulp.task('images', ['favicon'], function () {
  return gulp
    .src(src + 'images/**/*.*')
    .pipe(newer(dest + 'images/'))
    .pipe(imagemin())
    .pipe(gulp.dest(dest + 'images/'));
});

// manage favicon
gulp.task('favicon', function () {
  return gulp
    .src(src + 'favicon.ico')
    .on('error', interceptErrors)
    .pipe(newer(dest+ 'favicon.ico'))
    .pipe(gulp.dest(dest));
});

//Manage html template
gulp.task('views', function () {
  return gulp
    .src(viewFiles)
    .pipe(templateCache({standalone: true}))
    .on('error', interceptErrors)
    .pipe(rename('app.templates.js'))
    .pipe(gulp.dest('src/config'));
});

gulp.task('sass', function () {
  return gulp
    .src(sassFiles)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on('error', interceptErrors)
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(dest+'styles'));
});

//Use eslint to display errors
gulp.task('lint', () => {
  return gulp
    .src([src + '**/*.js','!./src/config/app.templates.js'])
    .pipe(eslint())
    .pipe(eslint.format())
});

// This task is used for building production ready minified JS/CSS files into
// the dist/ folder
gulp.task('build', ['html', 'sass','images', 'browserify'],
function () {
  var html = gulp
    .src('build/index.html')
    .pipe(gulp.dest(dist));

  var js = gulp
    .src('build/main.js')
    .pipe(uglify())
    .pipe(gulp.dest(dist));

  var sass = gulp
    .src('build/styles/**/*')
    .pipe(uglifycss({"maxLineLen": 500, "uglyComments": true}))
    .pipe(gulp.dest(dist + 'styles'));

  var img = gulp
    .src('build/images/**/*')
    .pipe(uglify())
    .pipe(gulp.dest(dist+'images'));

  return merge(html, js, sass, img);
});

gulp.task('default', [ 'html', 'images', 'sass', 'browserify' ],
function () {

  browserSync.init([dest + '**/**.**'], {
    server: dest,
    port: 4000,
    notify: false,
    ui: {
      port: 4001
    }
  });

  gulp.watch(src + 'index.html', ['html', 'images']);
  gulp.watch(viewFiles, ['views']);
  gulp.watch(sassFiles, ['sass']);
  gulp.watch(jsFiles, ['browserify']);
});
