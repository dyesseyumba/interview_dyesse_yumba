module.exports = function (config) {
  config.set({

    basePath: 'src',
    frameworks: [
      'browserify', 'jasmine'
    ],

    files: ['./**/*.js'],

    exclude: [],

    preprocessors: {
      './**/*.js': ['browserify']
    },

    browserify: {
      debug: true,
      transform: ['babelify']
    },

    client: {
      mocha: {
        reporter: 'html', // change Karma's debug.html to mocha web reporter
        ui: 'bdd'
      }
    },
    coverageReporter: {
      reporters: [
        {
          type: 'lcov',
          dir: '../coverage',
          subdir: '.'
        }, {
          type: 'text-summary'
        }
      ],
      includeAllSources: true
    },

    browsers: ['PhantomJS'],
    reporters: ['mocha', 'coverage'],
    concurrency: Infinity, port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    singleRun: true
  });
};
